require 'test_helper'

class RelationshipTest < ActiveSupport::TestCase
  
  def setup
    @releationship = Relationship.new(follower_id: 1, followed_id: 2)
  end
  
  test "should be valid" do
    assert @releationship.valid?
  end
  
  test "should require a follower_id" do
    @releationship.follower_id = nil
    assert_not @releationship.valid?
  end
  
  test "should require a followed_id" do
    @releationship.followed_id = nil
    assert_not @releationship.valid?
  end
  
end
